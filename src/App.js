import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LandingPage from './components/LandingPage';
import CharacterPage from './components/CharacterPage';
import ComicPage from './components/ComicPage';
import { getAPICharacters } from './components/requests';

const App = () => {
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  const getCharNames = async (search) => {
    if (search) {
      setLoading(true);
      const response = await getAPICharacters(search);
      setOptions(response);
      setLoading(false);
    } else {
      setOptions([]);
    }
  };
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <LandingPage
            options={options}
            loading={loading}
            requests={getCharNames}
          />
        </Route>
        <Route exact path="/character/:charId">
          <CharacterPage
            options={options}
            loading={loading}
            requests={getCharNames}
          />
        </Route>
        <Route exact path="/comic/:comicId">
          <ComicPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
