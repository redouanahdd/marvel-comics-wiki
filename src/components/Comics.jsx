/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import CardColumns from 'react-bootstrap/CardColumns';
import Spinner from 'react-bootstrap/Spinner';
import { getAPIComicsInfo } from './requests';
import Comic from './Comic';

const Comics = (props) => {
  const { characterId } = props;
  const [comics, setComics] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const getComicResources = async () => {
    try {
      setLoading(true);
      const comicsResponse = await getAPIComicsInfo(characterId);
      setComics(comicsResponse);
      console.log('comicsResponse', comicsResponse);
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    getComicResources();
  }, []);
  if (error) return 'Failed to load resource...';
  if (loading) {
    return <Spinner animation="border" variant="light" />;
  }
  return (
    <CardColumns>
      {comics?.length > 0 &&
        comics?.map((c) => {
          const coverUrl = `${c.thumbnail.path}.${c.thumbnail.extension}`;
          return (
            <Comic
              title={c.title}
              cover={coverUrl}
              description={c.description}
              id={c.id}
            />
          );
        })}
    </CardColumns>
  );
};

export default Comics;
