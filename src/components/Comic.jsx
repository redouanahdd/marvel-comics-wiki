/* eslint-disable react/prop-types */
import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

const Comic = (props) => {
  const { title, cover, description, id } = props;
  const shortDescription = (string) => {
    if (string && string.length > 0) {
      return `${string.substring(0, 150)}...`;
    }
    return 'No Description found for this comic...';
  };
  return (
    <Link
      to={`/comic/${id}`}
      style={{ textDecoration: 'none', color: 'black' }}
    >
      <Card className="comicCard">
        <Card.Img src={cover} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{shortDescription(description)}</Card.Text>
        </Card.Body>
      </Card>
    </Link>
  );
};

export default Comic;
