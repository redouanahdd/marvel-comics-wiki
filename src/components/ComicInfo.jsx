/* eslint-disable react/prop-types */
import React from 'react';
import { Card } from 'react-bootstrap';

const ComicInfo = (props) => {
  const { thumbnail, title, description } = props;
  return (
    <Card className="box-shadow charactercard">
      <Card.Img variant="top" src={thumbnail} />
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{description}</Card.Text>
      </Card.Body>
    </Card>
  );
};

export default ComicInfo;
