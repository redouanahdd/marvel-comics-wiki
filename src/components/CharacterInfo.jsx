/* eslint-disable react/prop-types */
import React from 'react';
import { Card, Button, Breadcrumb } from 'react-bootstrap';

const CharacterInfo = (props) => {
  const {
    thumbnail,
    name,
    description,
    comicsTotal,
    storiesTotal,
    seriesTotal,
  } = props;
  return (
    <Card className="box-shadow charactercard">
      <Card.Img variant="top" src={thumbnail} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Button href="https://www.marvel.com/" variant="primary">
          Learn More
        </Button>
        <Breadcrumb>
          <Breadcrumb.Item active>Comics: {comicsTotal}</Breadcrumb.Item>
          <Breadcrumb.Item active>Stories: {storiesTotal}</Breadcrumb.Item>
          <Breadcrumb.Item active>Series: {seriesTotal}</Breadcrumb.Item>
        </Breadcrumb>
      </Card.Body>
    </Card>
  );
};

export default CharacterInfo;
