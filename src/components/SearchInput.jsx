/* eslint-disable react/prop-types */
import React, { useState, useCallback } from 'react';
import debounce from 'lodash.debounce';
import { useHistory } from 'react-router-dom';
import { Input, Ul, Li, SuggestContainer } from './style';

const SearchInput = (props) => {
  const history = useHistory();
  const handleOnClick = (param) => {
    history.push(`/character/${param}`);
    window.location.reload();
  };
  const { loading, options, requests, placeholder } = props;
  const [inputValue, setInputValue] = useState('');

  const debouncedValue = useCallback(
    debounce((newValue) => requests(newValue), 1000),
    []
  );
  const updateValue = (newValue) => {
    setInputValue(newValue);
    debouncedValue(newValue);
  };
  return (
    <>
      <Input
        value={inputValue}
        onChange={(input) => updateValue(input.target.value)}
        placeholder={placeholder}
      />
      <SuggestContainer>
        <Ul>
          {loading && <Li>Loading...</Li>}
          {options?.length > 0 &&
            !loading &&
            options?.map((value) => (
              <Li key={`${value.id}`} onClick={() => handleOnClick(value.id)}>
                {value.name}
              </Li>
            ))}
        </Ul>
      </SuggestContainer>
    </>
  );
};

export default SearchInput;
