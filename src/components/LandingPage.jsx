/* eslint-disable react/prop-types */
import React from 'react';
import { Container, Jumbotron } from 'react-bootstrap';
import SearchInput from './SearchInput';

const LandingPage = (props) => {
  const { loading, options, requests } = props;
  return (
    <>
      <Container fluid>
        <Jumbotron fluid>
          <Container className="d-flex flex-column">
            <h1 className="headerTitle">Marvel Character Wiki</h1>{' '}
            <SearchInput
              loading={loading}
              options={options}
              requests={requests}
              placeholder="Search for a Marvel Character"
            />
          </Container>
        </Jumbotron>
      </Container>
    </>
  );
};

export default LandingPage;
