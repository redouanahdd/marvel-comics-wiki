/* eslint-disable import/prefer-default-export */
import md5 from 'md5';

const apiUrl = 'https://gateway.marvel.com:443/v1/public';
const publicKey = '1eae883a2c62fe1daf14e7a51f28b474';
const privateKey = '16e0352df3787b757084a5d455d61e34ec0a69d0';
const timestamp = Date.now();
const hash = md5(timestamp + privateKey + publicKey);

export const getAPICharacters = (search) => {
  const result = fetch(
    `${apiUrl}/characters?apikey=${publicKey}&ts=${timestamp}&hash=${hash}&limit=10&nameStartsWith=${search}`
  )
    .then((response) => response.json())
    .then((response) => response.data.results)
    .catch((error) => error);

  return result;
};

export const getAPICharInfo = (id) => {
  const result = fetch(
    `${apiUrl}/characters/${id}?apikey=${publicKey}&ts=${timestamp}&hash=${hash}`
  )
    .then((response) => response.json())
    .then((response) => response.data.results)
    .catch((error) => error);
  return result;
};

export const getAPIComicsInfo = (id) => {
  const result = fetch(
    `${apiUrl}/comics?formatType=comic&noVariants=true&characters=${id}&orderBy=-modified&limit=10&apikey=${publicKey}&ts=${timestamp}&hash=${hash}`
  )
    .then((response) => response.json())
    .then((response) => response.data.results)
    .catch((error) => error);
  return result;
};
export const getAPIComicInfoByComicId = (id) => {
  const result = fetch(
    `${apiUrl}/comics/${id}?apikey=${publicKey}&ts=${timestamp}&hash=${hash}`
  )
    .then((response) => response.json())
    .then((response) => response.data.results)
    .catch((error) => error);
  return result;
};
