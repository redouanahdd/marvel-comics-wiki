/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { Container, Jumbotron, Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { getAPICharInfo } from './requests';
import CharacterInfo from './CharacterInfo';
import Comics from './Comics';
import SearchInput from './SearchInput';

const CharacterPage = (props) => {
  const { options, requests } = props;
  const { charId } = useParams();
  const [charInfo, setCharInfo] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const getResources = async () => {
    try {
      setLoading(true);
      const info = await getAPICharInfo(charId);
      setCharInfo(info[0]);
      console.log('info', info);
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getResources();
  }, []);
  if (error) return 'Failed to load resource...';
  if (loading) {
    return <Spinner animation="border" variant="light" />;
  }
  const imageURL = `${charInfo.thumbnail.path}.${charInfo.thumbnail.extension}`;
  return (
    <>
      <Container fluid="lg">
        <Jumbotron fluid>
          <Container className="d-flex flex-column">
            <h1 className="headerTitle">Marvel Character Wiki</h1>{' '}
            <SearchInput
              loading={loading}
              options={options}
              requests={requests}
              placeholder="Search for a Marvel Character"
            />
          </Container>
        </Jumbotron>
        <h2 className="subHeader">Character</h2>
        <CharacterInfo
          thumbnail={imageURL}
          name={charInfo.name}
          description={charInfo.description}
          comicsTotal={charInfo.comics.available}
          storiesTotal={charInfo.stories.available}
          seriesTotal={charInfo.series.available}
        />
        <h2 className="subHeader">Comics</h2>
        <Comics characterId={charInfo.id} />
      </Container>
    </>
  );
};

export default CharacterPage;
