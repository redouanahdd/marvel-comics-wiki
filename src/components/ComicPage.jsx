import React, { useState, useEffect } from 'react';
import { Container, Spinner } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import ComicInfo from './ComicInfo';
import { getAPIComicInfoByComicId } from './requests';

const ComicPage = () => {
  const [comicInfo, setComicInfo] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const { comicId } = useParams();

  const getResources = async () => {
    try {
      setLoading(true);
      const info = await getAPIComicInfoByComicId(comicId);
      setComicInfo(info[0]);
      console.log('comicinfo', info);
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getResources();
  }, []);
  if (error) return 'Failed to load resource...';
  if (loading) {
    return <Spinner animation="border" variant="light" />;
  }
  const imageURL = `${comicInfo.thumbnail.path}.${comicInfo.thumbnail.extension}`;
  return (
    <Container fluid="lg">
      <h2 className="subHeader">Comic</h2>
      <ComicInfo
        thumbnail={imageURL}
        title={comicInfo.title}
        description={comicInfo.description}
      />
    </Container>
  );
};

export default ComicPage;
